import org.scalafmt.sbt.ScalafmtPlugin.autoImport.{scalafmtCheckAll, scalafmtConfig}
import sbt._
import sbt.Keys._
import sbtassembly.AssemblyKeys.assembly
import scoverage.ScoverageKeys.{
  coverageEnabled,
  coverageFailOnMinimum,
  coverageMinimum,
  coverageReport
}

object Build {

  val scalacFlags: Seq[String] = Seq(
    "-target:jvm-1.8",
    "-deprecation",
    "-unchecked",
    "-encoding",
    "UTF-8",
    "-Ywarn-numeric-widen",
    "-Ywarn-value-discard",
    "-Ywarn-dead-code",
    "-Ywarn-unused:_",
    "-Xdisable-assertions",
    "-Xfatal-warnings",
    "-Xlint:_",
    "-Xverify",
    "-feature",
    "-language:implicitConversions",
    "-language:existentials",
    "-language:postfixOps",
    "-language:higherKinds"
  )

  private val basicSettings: Seq[Def.Setting[_]] = Seq(
    organization := "com.gitlab.panos",
    scalacOptions ++= scalacFlags
  )

  private val coverageSettings: Seq[Def.Setting[_]] = Seq(
    ThisBuild / coverageEnabled := true,
    coverageMinimum := 22,
    coverageFailOnMinimum := true,
    Test / test := (coverageReport dependsOn Test / test).value
  )

  private val styleSettings: Seq[Def.Setting[_]] = Seq(
    scalafmtConfig := file(".scalafmt.conf"),
    // Check format before compilation.
    Compile / compile := (Compile / compile dependsOn scalafmtCheckAll).value
  )

  private val assemblySettings: Seq[Def.Setting[_]] = Seq(
    // Deactivate tests when running assembly.
    test in assembly := {}
  )

  val projectSettings: Seq[Def.Setting[_]] =
    Defaults.defaultConfigs
      .++(basicSettings)
      .++(styleSettings)
      .++(coverageSettings)
      .++(assemblySettings)
      .++(Defaults.itSettings)
}
