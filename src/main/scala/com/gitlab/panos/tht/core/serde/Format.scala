package com.gitlab.panos.tht.core.serde

/** Represents a serialization/deserialization format. */
sealed trait Format

object Format {

  /** Smart constructor for [[Format]] types.
    * @param format the name of the format that will be created
    * @return a [[Format]] if the given format name corresponds to a [[Format]], otherwise an empty
    *         [[scala.Option]]
    */
  def apply(format: String): Option[Format] =
    format.toLowerCase match {
      case "json"    => Some(Json)
      case "parquet" => Some(Parquet)
      case _         => None
    }

  /** Represents the `parquet` format */
  case object Parquet extends Format

  /** Represents the `json` format */
  case object Json extends Format

  /** Converts the [[Format]] to the corresponding format name. */
  implicit def name(x: Format): String = x.getClass.getSimpleName.toLowerCase.replaceAll("\\$", "")
}
