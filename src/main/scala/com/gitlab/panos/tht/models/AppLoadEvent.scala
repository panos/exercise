package com.gitlab.panos.tht.models

import java.sql.Timestamp

/** Represents the raw event schema of `app_load` type.
  * @param timestamp the time when the event occurred in ISO-8601 format
  * @param event the event type
  * @param initiator_id the user identity
  * @param device_type the user's device type
  */
case class AppLoadEvent(
  override val timestamp: Timestamp,
  override val event: String,
  override val initiator_id: String,
  device_type: String)
  extends RawEvent(timestamp, event, initiator_id)

object AppLoadEvent {

  /** The name that distinguishes the app load events. */
  val eventName: String = "app_loaded"

  /** Converts a raw event to the final format.
    * @param event the raw event
    * @return the final format based on the raw event
    */
  implicit def toSpec(event: AppLoadEvent): AppLoad =
    AppLoad(
      time = event.timestamp,
      initiator_id = event.initiator_id,
      device_type = event.device_type
    )
}

/** Represents the final format of the `app_load` event type.
  * @param time alias of [[AppLoadEvent.timestamp]]
  * @param initiator_id similar to [[AppLoadEvent.initiator_id]]
  * @param device_type similar to [[AppLoadEvent.device_type]]
  */
case class AppLoad(
  time: Timestamp,
  initiator_id: String,
  device_type: String)
