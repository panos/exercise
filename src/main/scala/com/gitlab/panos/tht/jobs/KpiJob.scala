package com.gitlab.panos.tht.jobs

import com.gitlab.panos.tht.core.CoreLogger
import com.gitlab.panos.tht.core.sources.Source
import com.gitlab.panos.tht.core.sinks.Sink
import com.gitlab.panos.tht.models.{AppLoad, Registration}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, count, lit, weekofyear}

/** Represents the job that extracts computes the metric from the two derived datasets */
object KpiJob extends CoreLogger {

  /** Executes the job.
    * @param registeredSource the source of the dataset with registered events
    * @param appLoadedSource the source of the dataset with app_loaded events
    * @param sink the sink in which the metric will be written
    */
  def run(registeredSource: Source, appLoadedSource: Source, sink: Sink): Unit = {
    logger.info("Kpi job is starting...")

    implicit val spark: SparkSession = SparkSession.builder.getOrCreate()

    import com.gitlab.panos.tht.core.implicits.ImplicitWriter
    import spark.implicits._

    // Define column names which are used only in this job.
    val idCol = "id"     // Represents the column initiator_id
    val timeCol = "time" // Represents the column time
    val weekCol = "week" // Represents the week of year based on time column

    // The name of the output column.
    val outputCol = "Percentage of users who loaded the app one week after the registration"

    // Read registered events and create a local DF with the key that will be used in grouping.
    // Cached so as to calculate only once.
    val registered =
      registeredSource.read
        .as[Registration]
        .map(e => (e.initiator_id, e.time))
        .toDF(idCol, timeCol)
        .cache()

    // Group registered DF based on the initiator_id and week of year
    val registeredGrouped =
      registered
        .groupBy(col(idCol), weekofyear(col(timeCol)) as weekCol)
        .count()

    // Read app_loaded events and create a local DF. Group by initiator_id and `week of year - 1`.
    // 1 is subtracted from week as we are looking app load events that happened one week after the
    // user's registration.
    val appLoadedGrouped = appLoadedSource.read
      .as[AppLoad]
      .map(e => (e.initiator_id, e.time))
      .toDF(idCol, timeCol)
      .groupBy(col(idCol), weekofyear(col(timeCol)) - lit(1) as weekCol)
      .count()

    // Compute the number of users who loaded one week after the registration.
    val numOfUsersWhoLoadedNextWeek =
      registeredGrouped
        .join(appLoadedGrouped, Seq(idCol, weekCol))
        .count()

    // Compute the percentage and print
    registered
      .select((lit(numOfUsersWhoLoadedNextWeek) / count("*")) * lit(100) as outputCol)
      .to(sink)

    logger.info("Kpi job finished")
  }
}
