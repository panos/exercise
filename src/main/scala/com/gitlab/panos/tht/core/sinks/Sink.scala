package com.gitlab.panos.tht.core.sinks

import org.apache.spark.sql.Dataset

/** Represents a sink. Sinks perform an output operation. */
trait Sink {

  /** Performs the output operation on the given dataset.
    * @param ds the dataset that will be processed
    * @tparam T the type of the dataset
    */
  def write[T](ds: Dataset[T]): Unit
}
