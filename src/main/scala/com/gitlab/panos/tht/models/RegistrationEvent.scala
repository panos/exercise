package com.gitlab.panos.tht.models

import java.sql.Timestamp

/**
  * @param timestamp the time when the event occurred in ISO-8601 format
  * @param event the event type
  * @param initiator_id the user identity
  * @param channel the acquisition channel
  */
case class RegistrationEvent(
  override val timestamp: Timestamp,
  override val event: String,
  override val initiator_id: String,
  channel: String)
  extends RawEvent(timestamp, event, initiator_id)

object RegistrationEvent {

  /** The name that distinguishes the registered events. */
  val eventName: String = "registered"

  /** Converts a raw event to the final format.
    * @param event the raw event
    * @return the final format based on the raw event
    */
  implicit def toSpec(event: RegistrationEvent): Registration =
    Registration(
      time = event.timestamp,
      initiator_id = event.initiator_id,
      channel = event.channel
    )
}

/** Represents the final format of the `registered` event type.
  * @param time alias of [[RegistrationEvent.timestamp]]
  * @param initiator_id similar to [[RegistrationEvent.initiator_id]]
  * @param channel similar to [[RegistrationEvent.channel]]
  */
case class Registration(
  time: Timestamp,
  initiator_id: String,
  channel: String)
