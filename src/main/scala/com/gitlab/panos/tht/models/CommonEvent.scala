package com.gitlab.panos.tht.models

import java.sql.Timestamp

/** Represents the common schema of events.
  * @param timestamp the time when the event occurred in ISO-8601 format
  * @param event the event type
  * @param initiator_id the user's device type
  */
case class CommonEvent(
  override val timestamp: Timestamp,
  override val event: String,
  override val initiator_id: String)
  extends RawEvent(timestamp, event, initiator_id)
