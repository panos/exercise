package com.gitlab.panos.tht.jobs

import java.nio.file.Paths

import com.gitlab.panos.tht.CoreISpec
import com.gitlab.panos.tht.core.SparkSystem
import com.gitlab.panos.tht.core.serde.Format
import com.gitlab.panos.tht.core.sinks.FileSink
import com.gitlab.panos.tht.core.sources.FileSource
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import scala.reflect.io.Directory

class KpiJobITest extends CoreISpec {
  "A KpiJob" should "compute the percentage of users who loaded the app one week after their registration" in {
    Given("a FileSource for registered events")
    val registeredSource = new FileSource(KpiJobITest.registeredPath, Format.Parquet)

    And("a FileSource for app_loaded events")
    val appLoadedSource = new FileSource(KpiJobITest.appLoadedPath, Format.Parquet)

    // Use FileSink so we can assert
    And("a Sink")
    val sink = new FileSink(KpiJobITest.outputPath, Format.Json)

    When("the KpiJob runs")
    Then("the metric should be put to the given sink")
    SparkSystem(new SparkConf().setAppName("kpi_job_test").setMaster("local[1]")) { sc =>
      KpiJob.run(registeredSource, appLoadedSource, sink)

      val spark = SparkSession.builder.getOrCreate()

      // Only user 1 and 2 loaded one week after the registration.
      // User 3 loaded only the week in which they registered.
      // So result should be (2/3) * 100
      spark.read.json(KpiJobITest.outputPath).collect.head.get(0) shouldBe 66.66666666666666

      sc.stop()

      // Clean up files
      val _ = Directory(Paths.get(KpiJobITest.outputPath).toFile).deleteRecursively()
    }

  }
}

/** Contains the fixtures */
object KpiJobITest {
  val registeredPath = Paths.get(getClass.getResource("/").getPath, "registered").toUri.getPath

  val appLoadedPath = Paths.get(getClass.getResource("/").getPath, "app_loaded").toUri.getPath

  val outputPath = Paths.get(getClass.getResource("/").getPath, "result").toUri.getPath
}
