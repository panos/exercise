package com.gitlab.panos.tht.core.sources

import org.apache.spark.sql.{DataFrame, SparkSession}

/** Represents a source. Sources perform an input operation. */
trait Source {

  /** Extracts data from the source.
    * @param spark the session that will be used to read the [[DataFrame]]
    * @return the data that were extracted from the source
    */
  def read()(implicit spark: SparkSession): DataFrame
}
