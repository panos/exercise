package com.gitlab.panos.tht

import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

trait CoreISpec extends AnyFlatSpecLike with GivenWhenThen with Matchers
