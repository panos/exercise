package com.gitlab.panos.tht.core.transformers

import com.gitlab.panos.tht.{CoreSpec, SparkSpec, Tags}
import org.apache.spark.sql.SparkSession

class RepartitionerTest extends CoreSpec with SparkSpec {
  "A Repartitioner" should "repartition a DF only if the provided partitions are not empty" taggedAs Tags.unit in {
    val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._

    // Creates a DF with the test's default partitions
    val df = sc.parallelize(Seq(("one", 1), ("two", 2)), RepartitionerTest.defaultPartitions).toDF

    val newPartitions = 1

    val shouldNotBeRepartitioned = Repartitioner(None)(df)
    val shouldBeRepartitioned = Repartitioner(Some(newPartitions))(df)

    shouldNotBeRepartitioned.rdd.partitions should have size RepartitionerTest.defaultPartitions.toLong
    shouldBeRepartitioned.rdd.partitions should have size newPartitions.toLong
  }
}

object RepartitionerTest {
  val defaultPartitions: Int = 2
}
