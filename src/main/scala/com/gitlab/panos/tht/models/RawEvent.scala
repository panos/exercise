package com.gitlab.panos.tht.models

import java.sql.Timestamp

/** Represents the common schema of an unprocessed event.
  * @param timestamp the time when the event occurred in ISO-8601 format
  * @param event the event type
  * @param initiator_id the user identity
  */
abstract class RawEvent(val timestamp: Timestamp, val event: String, val initiator_id: String)
