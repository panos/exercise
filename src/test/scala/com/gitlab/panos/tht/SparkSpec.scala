package com.gitlab.panos.tht

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterEach, Suite}

import scala.reflect.io.File

trait SparkSpec extends BeforeAndAfterEach { this: Suite =>

  Logger.getLogger("org").setLevel(Level.OFF)

  var sc: SparkContext = _

  override def beforeEach(): Unit = {
    // Clean the locks
    File("./metastore_db/db.lck").delete()
    File("./metastore_db/dbex.lck").delete()

    super.beforeEach()

    val conf = new SparkConf()
      .setMaster("local[4]")
      .setAppName(this.getClass.getSimpleName)

    sc = new SparkContext(config = conf)
    sc.setLogLevel("OFF")
  }

  override def afterEach(): Unit = {
    sc.stop()
    super.afterEach()
  }
}
