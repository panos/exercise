package com.gitlab.panos.tht.core.sinks

import com.gitlab.panos.tht.core.serde.Format
import org.apache.spark.sql.Dataset

/** Writes datasets to the provided path in the provided format. The provided `options` will be
  * passed to [[org.apache.spark.sql.DataFrameWriter]].
  * @param path the path in which data will be saved to
  * @param format the format of the data that will be saved
  * @param options similar to [[org.apache.spark.sql.DataFrameWriter.options]]
  */
class FileSink(path: String, format: Format, options: Map[String, String] = Map.empty)
  extends Sink {

  override def write[T](ds: Dataset[T]): Unit = ds.write.options(options).format(format).save(path)
}
