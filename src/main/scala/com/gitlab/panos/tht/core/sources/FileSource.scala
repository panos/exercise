package com.gitlab.panos.tht.core.sources

import com.gitlab.panos.tht.core.serde.Format
import org.apache.spark.sql.{DataFrame, SparkSession}

/** Reads data from the provided path based on the provided format. The provided options will be
  * passed to [[org.apache.spark.sql.DataFrameReader]]
  * @param path the path where the data are saved
  * @param format the format that the date have
  * @param options similar to [[org.apache.spark.sql.DataFrameReader.options]]
  */
class FileSource(path: String, format: Format, options: Map[String, String] = Map.empty)
  extends Source {

  override def read()(implicit spark: SparkSession): DataFrame =
    spark.read.options(options).format(format).load(path)
}
