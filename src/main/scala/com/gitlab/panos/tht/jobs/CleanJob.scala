package com.gitlab.panos.tht.jobs

import com.gitlab.panos.tht.core.CoreLogger
import com.gitlab.panos.tht.core.sources.Source
import com.gitlab.panos.tht.core.sinks.Sink
import com.gitlab.panos.tht.models.{
  AppLoad,
  AppLoadEvent,
  CommonEvent,
  Registration,
  RegistrationEvent
}
import org.apache.spark.sql.{Dataset, SparkSession}

/** Represents the job that extracts the two datasets from the raw dataset */
object CleanJob extends CoreLogger {

  /** Executes the job.
    * @param source the source of the input raw dataset
    * @param registeredSink the sink in which the registered dataset will be saved
    * @param appLoadedSink the sink in which the app_loaded dataset will be saved
    * @param numRegisteredFiles the number of files that the registered dataset will have. If it is
    *                           empty the default partitions will be kept
    * @param numAppLoadFiles the number of files that the app_loaded dataset will have. If it is
    *                        empty the default partitions will be kept.
    */
  def run(
    source: Source,
    registeredSink: Sink,
    appLoadedSink: Sink,
    numRegisteredFiles: Option[Int],
    numAppLoadFiles: Option[Int]
  ): Unit = {
    logger.info("Cleaning job is starting...")

    implicit val spark: SparkSession = SparkSession.builder.getOrCreate()

    import com.gitlab.panos.tht.core.implicits._
    import spark.implicits._

    // Cache raw dataset so as to read only once.
    val dataset: Dataset[CommonEvent] = source.read.as[CommonEvent].cache()

    logger.info("Processing Registration events")
    dataset
      .filter(_.event == RegistrationEvent.eventName) // keep only registered events
      .as[RegistrationEvent]                          // map to raw registration event schema
      .mapSchemaTo[Registration]                      // map to output schema
      .prune()                                        // keep only the columns defined in output schema
      .maybeRepartition(numRegisteredFiles)           // repartition if necessary
      .to(registeredSink)                             // save to sink

    logger.info("Processing App Load events")
    dataset
      .filter(_.event == AppLoadEvent.eventName) // keep only app_loaded events
      .as[AppLoadEvent]                          // map to raw app load event schema
      .mapSchemaTo[AppLoad]                      // map to output schema
      .prune()                                   // keep only the columns defined in output schema
      .maybeRepartition(numAppLoadFiles)         // repartition if necessary
      .to(appLoadedSink)                         // save to sink

    logger.info("Cleaning job finished")
  }
}
