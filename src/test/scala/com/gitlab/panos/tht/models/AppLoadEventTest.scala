package com.gitlab.panos.tht.models

import java.sql.Timestamp

import com.gitlab.panos.tht.{CoreSpec, Tags}

class AppLoadEventTest extends CoreSpec {
  "An AppLoadEvent" should "be converted to AppLoad" taggedAs Tags.unit in {
    val time = Timestamp.valueOf("2020-08-30 12:13:14")
    val deviceType = "Mobile"
    val initiatorId = "123"

    val appLoadEvent = AppLoadEvent(
      timestamp = time,
      event = "app_loaded",
      initiator_id = initiatorId,
      device_type = deviceType
    )

    val expectedAppLoad = AppLoad(time = time, initiator_id = initiatorId, device_type = deviceType)

    AppLoadEvent.toSpec(appLoadEvent) shouldBe expectedAppLoad
  }
}
