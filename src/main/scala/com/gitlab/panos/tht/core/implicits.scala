package com.gitlab.panos.tht.core

import com.gitlab.panos.tht.core.sinks.Sink
import com.gitlab.panos.tht.core.transformers.{Projector, Repartitioner}
import org.apache.spark.sql.{Dataset, Encoder}

/** Implicit custom operations on [[Dataset]]. */
object implicits {

  /** Uses [[Projector]] to select the fields defined in [[T]]
    * @param ds the dataset that will be projected
    * @param encoder$T$0 an implicit [[Encoder]] for the type of the given [[Dataset]]
    * @tparam T the type of the given [[Dataset]]
    */
  implicit class ImplicitProjector[T: Encoder](ds: Dataset[T]) {

    /** Selects only the fields defined in [[T]] */
    def prune(): Dataset[T] = Projector(ds)
  }

  /** Provides the `to` method that writes the [[Dataset]] to the given [[Sink]].
    * @param ds the dataset that will be used by the writer
    * @tparam T the type of the given [[Dataset]]
    */
  implicit class ImplicitWriter[T](ds: Dataset[T]) {

    /** Saves the [[Dataset]] to the given [[Sink]].
      * @param sink the sink in which the given [[Dataset]] will be saved
      */
    def to(sink: Sink): Unit = sink.write(ds)
  }

  /** Provides the `mapSchemaTo` method that maps the schema of the given [[Dataset]] to a new
    * Schema by applying a conversion function `f`.
    * @param ds the dataset that will be mapped to the new schema
    * @tparam T the type of the dataset
    */
  implicit class ImplicitMapper[T](ds: Dataset[T]) {

    /** Maps the schema of the given [[Dataset]] to a new schema by applying the given function `f`.
      * @param f the function that will be used to convert the schema
      * @tparam U the new type
      * @return a [[Dataset]] with the new schema
      */
    def mapSchemaTo[U: Encoder]()(implicit f: T => U): Dataset[U] = ds.map(f)
  }

  /** Provides the `maybeRepartition` which uses [[Repartitioner]] to repartition the given dataset.
    * @param ds the dataset that will may be repartitioned
    * @tparam T the type of the given dataset
    */
  implicit class ImplicitRepartitioner[T](ds: Dataset[T]) {

    /** Uses [[Repartitioner]] to repartition the dataset.
      * @param p the number of new partitions
      * @return the given dataset with the default number of partitions if `p` is empty, otherwise
      *         with `p` number of partitions.
      */
    def maybeRepartition(p: Option[Int]): Dataset[T] = Repartitioner(p)(ds)
  }
}
