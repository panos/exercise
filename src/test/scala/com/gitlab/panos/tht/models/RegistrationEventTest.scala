package com.gitlab.panos.tht.models

import java.sql.Timestamp

import com.gitlab.panos.tht.{CoreSpec, Tags}

class RegistrationEventTest extends CoreSpec {
  "A RegistrationEvent" should "be converted to Registration" taggedAs Tags.unit in {
    val time = Timestamp.valueOf("2020-08-30 12:13:14")
    val initiatorId = "123"
    val channel = "invite"

    val registrationEvent = RegistrationEvent(
      timestamp = time,
      initiator_id = initiatorId,
      channel = channel,
      event = "registered"
    )

    val expectedRegistration =
      Registration(time = time, initiator_id = initiatorId, channel = channel)

    RegistrationEvent.toSpec(registrationEvent) shouldBe expectedRegistration
  }
}
