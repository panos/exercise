package com.gitlab.panos.tht.core.sinks

import java.nio.file.Paths

import com.gitlab.panos.tht.core.serde.Format
import com.gitlab.panos.tht.{CoreSpec, SparkSpec, Tags}
import org.apache.spark.sql.SparkSession
import org.scalatest.BeforeAndAfterEach

import scala.reflect.io.Directory

class FileSinkTest extends CoreSpec with SparkSpec with BeforeAndAfterEach {

  override def afterEach(): Unit = {
    super.afterEach()
    // Cleanup dir
    val _ = Directory(Paths.get(FileSinkTest.path).toFile).deleteRecursively()
  }

  "A FileSink" should "write a dataset to a directory as a file" taggedAs Tags.unit in {
    val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._
    // Create a dummy dataframe (it matches the dummy_dataset.json in resources)
    val ds = sc.parallelize(Seq(("one", 1), ("two", 2))).toDF("key1", "key2")
    // Create the sink which will be tested
    val sink = new FileSink(FileSinkTest.path, Format.Json)
    sink write ds
    // Assert. As the dummy DF we created matches the DF in resources directory we can check that
    // these two are the same.
    spark.read.json(FileSinkTest.path).collect() should contain theSameElementsAs ds.collect()
  }
}

object FileSinkTest {
  val path = Paths.get(getClass.getResource("/").getPath, "file_sink_test").toUri.getPath
}
