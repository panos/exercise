package com.gitlab.panos.tht

import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

trait CoreSpec extends AnyFlatSpecLike with Matchers
