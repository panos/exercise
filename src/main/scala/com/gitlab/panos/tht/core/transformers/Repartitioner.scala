package com.gitlab.panos.tht.core.transformers

import com.gitlab.panos.tht.core.CoreLogger
import org.apache.spark.sql.Dataset

/** Changes the number of partition of a [[Dataset]] */
object Repartitioner extends CoreLogger {

  /** Repartitions the provided dataset only if `partitions` is defined, otherwise the input dataset
    * will be returned.
    * @param partitions the number of partitions that the new dataset will have. If it's empty the
    *                   dataset will no be repartitioned.
    * @param ds the dataset that will be repartitioned
    * @tparam T the dataset's type
    * @return a repartitioned dataset if `partitions` is defined, otherwise the input dataset.
    */
  def apply[T](partitions: Option[Int])(ds: Dataset[T]): Dataset[T] =
    partitions.map(ds.repartition).getOrElse(ds)
}
