package com.gitlab.panos.tht.cli

import com.gitlab.panos.tht.core.serde.Format
import com.gitlab.panos.tht.core.{CoreLogger, SparkSystem}
import com.gitlab.panos.tht.core.sources.FileSource
import com.gitlab.panos.tht.core.sinks.{ConsoleSink, FileSink}
import com.gitlab.panos.tht.jobs.{CleanJob, KpiJob}
import org.apache.spark.SparkConf
import scopt.OptionParser

object Exercise extends CoreLogger {

  case class Config(
    job: Int = 1,
    inputFile: String = "",
    registrationEventsDir: String = "",
    appLoadEventsDir: String = "",
    numFilesRegistrationEvents: Option[Int] = None,
    numFilesAppLoadEvents: Option[Int] = None,
    inputFormat: Format = Format.Json,
    registrationEventsFormat: Format = Format.Parquet,
    appLoadEventsFormat: Format = Format.Parquet,
    appName: String = "panos_exercise")

  def main(args: Array[String]): Unit = {
    parser.parse(args, Config()) match {
      case Some(config) =>
        val sparkConf = new SparkConf().setAppName(config.appName)

        SparkSystem(sparkConf) { _ =>
          config.job match {
            case 1 =>
              CleanJob.run(
                source = new FileSource(config.inputFile, config.inputFormat),
                registeredSink =
                  new FileSink(config.registrationEventsDir, config.registrationEventsFormat),
                appLoadedSink =
                  new FileSink(config.appLoadEventsDir, config.registrationEventsFormat),
                numRegisteredFiles = config.numFilesRegistrationEvents,
                numAppLoadFiles = config.numFilesAppLoadEvents
              )
            case 2 =>
              KpiJob.run(
                registeredSource =
                  new FileSource(config.registrationEventsDir, config.registrationEventsFormat),
                appLoadedSource =
                  new FileSource(config.appLoadEventsDir, config.appLoadEventsFormat),
                sink = new ConsoleSink()
              )
          }
        }

      case None =>
        logger.error("Malformed input arguments")
    }
  }

  val parser: OptionParser[Config] = new scopt.OptionParser[Config]("exercise") {
    head("Panos Bletsos", "exercise")

    implicit val formatReader: scopt.Read[Option[Format]] = scopt.Read.reads(Format.apply)

    opt[Int]("job")
      .required()
      .valueName("<value>")
      .action((x, c) => c.copy(job = x))
      .validate(x => if (x == 1 || x == 2) success else failure("job can be 1 or 2"))
      .text("the job number to execute")

    opt[String]("registration-events-dir")
      .required()
      .valueName("<dir>")
      .action((x, c) => c.copy(registrationEventsDir = x))
      .text("directory in which registration events will be saved")

    opt[String]("app-load-events-dir")
      .required()
      .valueName("<dir>")
      .action((x, c) => c.copy(appLoadEventsDir = x))
      .text("directory in which app load events will be saved")

    opt[String]("input-file")
      .valueName("<file>")
      .action((x, c) => c.copy(inputFile = x))
      .text("path to input dataset")

    opt[Int]("num-files-registration-events")
      .valueName("<value>")
      .action((x, c) => c.copy(numFilesRegistrationEvents = Some(x)))
      .text("number of output files of registration events")

    opt[Int]("num-files-app-load-events")
      .valueName("<value>")
      .action((x, c) => c.copy(numFilesAppLoadEvents = Some(x)))
      .text("number of output files of app loaded events")

    opt[Option[Format]]("input-format")
      .valueName("<value>")
      .action((x, c) => c.copy(inputFormat = x.get))
      .validate(x => if (x.isDefined) success else failure("input-format value is not supported"))
      .text("input file format, e.g. json")

    opt[Option[Format]]("registration-events-format")(scopt.Read.reads(Format.apply))
      .valueName("<value>")
      .action((x, c) => c.copy(registrationEventsFormat = x.get))
      .validate(x =>
        if (x.isDefined) success
        else failure("registration-events-format value is not supported")
      )
      .text("output file format for registration events, e.g. json")

    opt[Option[Format]]("app-load-events-format")
      .valueName("<value>")
      .action((x, c) => c.copy(registrationEventsFormat = x.get))
      .validate(x =>
        if (x.isDefined) success
        else failure("registration-events-format value is not supported")
      )
      .text("output file format for app loaded events, e.g. json")

    opt[String]("app-name")
      .valueName("<value>")
      .action((x, c) => c.copy(appName = x))
      .text("Spark app name")

    checkConfig { config =>
      if (config.job == 1 && config.inputFile.isEmpty)
        failure("Argument input-file is required in job 1")
      else success
    }
  }
}
