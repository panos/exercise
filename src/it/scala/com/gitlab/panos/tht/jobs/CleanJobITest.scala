package com.gitlab.panos.tht.jobs

import java.nio.file.Paths

import com.gitlab.panos.tht.CoreISpec
import com.gitlab.panos.tht.core.SparkSystem
import com.gitlab.panos.tht.core.serde.Format
import com.gitlab.panos.tht.core.sinks.FileSink
import com.gitlab.panos.tht.core.sources.FileSource
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import scala.reflect.io.Directory

class CleanJobITest extends CoreISpec {
  "A CleanJob" should "extract two datasets from the raw dataset and save them as parquet" in {
    Given("a FileSource that reads json")
    val source = new FileSource(getClass.getResource("/dataset.json").getPath, Format.Json)

    And("a Sink for registered events")
    val registeredSink =
      new FileSink(CleanJobITest.registeredPath, Format.Parquet)

    And("a Sink for app_loaded events")
    val appLoadedSink = new FileSink(CleanJobITest.appLoadedPath, Format.Parquet)

    When("the job runs")
    Then("two datasets should be created")

    SparkSystem(new SparkConf().setAppName("clean_job_test").setMaster("local[1]")) { sc =>
      CleanJob.run(source, registeredSink, appLoadedSink, None, None)

      val spark = SparkSession.builder.getOrCreate()

      val registered = spark.read.parquet(CleanJobITest.registeredPath).collect()
      val appLoaded = spark.read.parquet(CleanJobITest.appLoadedPath).collect()

      registered should have size 3
      registered.map(_.getString(2)) should contain theSameElementsAs Array(
        "channel1",
        "channel2",
        null
      )

      appLoaded should have size 7
      appLoaded.map(_.getString(2)) should contain theSameElementsAs Array.fill(7)("desktop")

      sc.stop()
    }

    // Clean up
    val _ = {
      Directory(Paths.get(CleanJobITest.registeredPath).toFile).deleteRecursively() &&
      Directory(Paths.get(CleanJobITest.appLoadedPath).toFile).deleteRecursively()
    }
  }
}

object CleanJobITest {

  val registeredPath = Paths.get(getClass.getResource("/").getPath, "registered_test").toUri.getPath

  val appLoadedPath = Paths.get(getClass.getResource("/").getPath, "app_loaded_test").toUri.getPath
}
