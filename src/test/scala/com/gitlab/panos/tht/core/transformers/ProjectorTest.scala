package com.gitlab.panos.tht.core.transformers

import com.gitlab.panos.tht.{CoreSpec, SparkSpec, Tags}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

class ProjectorTest extends CoreSpec with SparkSpec {
  "A Projector" should "select only the columns defined in T" taggedAs Tags.unit in {
    val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._

    // The DF has one more column than the Dummy dataset.
    val ds = sc
      .parallelize(Seq(("one", 1, true), ("two", 2, false)))
      .toDF("key1", "key2", "key3")
      .as[ProjectorTest.Dummy]

    val projectedDS = Projector(ds).toDF.collect()

    val expectedSchema = new StructType(
      // Int cannot be null
      Array(StructField("key1", StringType, true), StructField("key2", IntegerType, false))
    )

    projectedDS should have size 2
    projectedDS.head.size shouldBe 2
    projectedDS.head.schema shouldBe expectedSchema
  }
}

object ProjectorTest {
  case class Dummy(key1: String, key2: Int)
}
