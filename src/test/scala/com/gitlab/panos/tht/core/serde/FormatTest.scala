package com.gitlab.panos.tht.core.serde

import com.gitlab.panos.tht.{CoreSpec, Tags}

class FormatTest extends CoreSpec {
  "A Format" should "be created given a format name" taggedAs Tags.unit in {
    val json = "json"
    val parquet = "parquet"
    val unknown = "__orc"

    val jsonFormat = Format(json)
    val parquetFormat = Format(parquet)
    val unknownFormat = Format(unknown)

    jsonFormat shouldBe 'defined
    jsonFormat map { _ shouldBe a[Format.Json.type] }

    parquetFormat shouldBe 'defined
    parquetFormat map { _ shouldBe a[Format.Parquet.type] }

    unknownFormat shouldBe 'empty
  }

  it should "be converted to the corresponding String" taggedAs Tags.unit in {
    val json: String = Format.Json
    val parquet: String = Format.Parquet

    json shouldBe "json"
    parquet shouldBe "parquet"
  }
}
