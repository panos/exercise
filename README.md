# Panos Bletsos | Data Engineer | Exercise

## Setup

### Prerequisites

To build and run the application the following dependencies should be installed
- Java 8
- Scala 2.12.12
- sbt 1.3.13
- Docker (optional)

### Build

To create the application's uber jar run
```commandline
sbt coverageOff clean assembly
```

### Tests

The project follows sbt's standard test location. So tests are located in:
- unit tests: `src/test/scala`
- integration tests: `src/it/scala`

#### Unit

Run unit tests with the following command
```commandline
sbt clean test
```

### Integration

Integration tests are testing the whole flow with a limited dataset.

Run integration tests with the following command
```commandline
sbt clean it:test
```

## Run

First of all you have to create the application's uber jar with the command we've already seen in [build](README.md#Build) section.

When the jar is ready you can use it to submit the application in a Spark cluster.

For convenience the project includes a Docker file which creates a cluster. To start the cluster run the following commands
```commandline
cd build/spark
docker-compose up
```

When the cluster is up and running you can submit the application.

### Application 1
To start the first job run

```commandline
docker exec -it spark_spark_1 spark-submit \
--master spark://spark:7077 \
--deploy-mode client \
--class com.gitlab.panos.tht.cli.Exercise /jars/panos-tht-assembly-0.1.jar \
--job 1 \
--registration-events-dir /data/events/registered \
--app-load-events-dir /data/events/app_loaded \
--input-file /data/dataset.json
```

When you run this command it will submit the application to the cluster and eventually the two datasets
will be created in the `data` directory.

### Application 2

To start the second job run

```commandline
docker exec -it spark_spark_1 spark-submit \
--master spark://spark:7077 \
--deploy-mode client \
--class com.gitlab.panos.tht.cli.Exercise /jars/panos-tht-assembly-0.1.jar \
--job 2 \
--registration-events-dir /data/events/registered \
--app-load-events-dir /data/events/app_loaded
```

When the job is finished the result will be printed on the console, so you will see
```
+----------------------------------------------------------------------+
|Percentage of users who loaded the app one week after the registration|
+----------------------------------------------------------------------+
|                                                     7.570374419240229|
+----------------------------------------------------------------------+
```

### Available parameters
You can print the available parameters by running

```commandline
docker exec -it spark_spark_1 spark-submit \
--master spark://spark:7077 \
--deploy-mode client \
--class com.gitlab.panos.tht.cli.Exercise /jars/panos-tht-assembly-0.1.jar
```

which will print

```
Panos Bletsos exercise
Usage: exercise [options]

  --job <value>            the job number to execute
  --registration-events-dir <dir>
                         directory in which registration events will be saved
  --app-load-events-dir <dir>
                           directory in which app load events will be saved
  --input-file <file>      path to input dataset
  --num-files-registration-events <value>
                           number of output files of registration events
  --num-files-app-load-events <value>
                           number of output files of app loaded events
  --input-format <value>   input file format, e.g. json
  --registration-events-format <value>
                           output file format for registration events, e.g. json
  --app-load-events-format <value>
                           output file format for app loaded events, e.g. json
  --app-name <value>       Spark app name
```
