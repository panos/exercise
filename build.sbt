import Build._

name := "panos-tht"

version := "0.1"

scalaVersion := "2.12.12"

libraryDependencies ++= Seq(
  "com.github.scopt" %% "scopt"      % "3.7.1",
  "org.apache.spark" %% "spark-core" % "3.0.0" % Provided,
  "org.apache.spark" %% "spark-sql"  % "3.0.0" % Provided,
  "org.scalatest"    %% "scalatest"  % "3.2.0" % "it,test"
)

lazy val root = (project in file(".")).configs(IntegrationTest).settings(projectSettings)

concurrentRestrictions in Global += Tags.limit(Tags.Test, 1)
