package com.gitlab.panos.tht.core.sinks

import org.apache.spark.sql.Dataset

/** Prints datasets to the console.
  * @param numOfRows number of rows to print. By default it will print 20 rows.
  */
class ConsoleSink(numOfRows: Int = 20) extends Sink {
  override def write[T](ds: Dataset[T]): Unit = ds.show(numOfRows)
}
