package com.gitlab.panos.tht.core

import org.apache.log4j.Logger

/** The logger that can be used in the project. */
trait CoreLogger extends Serializable {

  /** The logger with the class' name */
  @transient val logger: Logger = Logger.getLogger(this.getClass)
}
