package com.gitlab.panos.tht.core.transformers

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, Encoder}

/** Selects a set of columns */
object Projector {

  /** Selects the columns that are defined in [[T]]
    * @param dataset the dataset that will be used in the projection
    * @param encoder the encoder of [[T]]
    * @tparam T the type of the given dataset
    * @return a new [[Dataset]] which has only the columns which are defined in type [[T]]
    */
  def apply[T](dataset: Dataset[T])(implicit encoder: Encoder[T]): Dataset[T] =
    dataset.select(encoder.schema.fieldNames.map(col): _*).as[T]
}
