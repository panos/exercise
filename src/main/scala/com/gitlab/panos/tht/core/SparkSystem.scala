package com.gitlab.panos.tht.core

import org.apache.spark.{SparkConf, SparkContext}

/** Initializes Spark. */
object SparkSystem extends CoreLogger {

  /** Creates a [[SparkContext]] based on the given `conf` and executes the `f` given the created
    * context.
    * @param conf the configuration that will be used in the [[SparkContext]]
    * @param f the function that uses the provided [[SparkContext]]
    */
  def apply(conf: SparkConf)(f: SparkContext => Unit): Unit = {
    logger.info("Creating Spark Context")
    val sc = SparkContext.getOrCreate(conf)
    f(sc)
  }
}
