package com.gitlab.panos.tht.core.sources

import com.gitlab.panos.tht.{CoreSpec, SparkSpec, Tags}
import com.gitlab.panos.tht.core.serde.Format
import org.apache.spark.sql.SparkSession

class FileSourceTest extends CoreSpec with SparkSpec {
  "A FileSource" should "read a DataFrame from a path" taggedAs Tags.unit in {
    implicit val spark: SparkSession = SparkSession.builder.getOrCreate()
    val path = getClass.getResource("/dummy_dataset.json").getPath
    val source = new FileSource(path, Format.Json)
    val data = source.read.collect()

    data should have size 2
    data.head.get(0) shouldBe "one"
    data.last.get(0) shouldBe "two"
  }
}
